module gitlab.com/capi2/api

go 1.18

require (
	github.com/graphql-go/graphql v0.8.0 // indirect
	github.com/graphql-go/handler v0.2.3 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgx/v5 v5.2.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	golang.org/x/crypto v0.4.0 // indirect
	golang.org/x/text v0.5.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gorm.io/driver/postgres v1.4.7 // indirect
	gorm.io/driver/sqlite v1.4.4 // indirect
	gorm.io/gorm v1.24.5 // indirect
)
