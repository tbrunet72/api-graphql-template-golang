package queries

import (
	"errors"

	"github.com/graphql-go/graphql"
	"gitlab.com/capi2/api/database"
	"gitlab.com/capi2/api/types"
)

// GetUserQuery returns the queries available against user type.
func GetUserQuery() *graphql.Field {
	return &graphql.Field{
		Type:        graphql.NewList(types.UserType),
		Description: "Get all users",
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			db, err := database.GetDatabase()
			if err != nil {
				return nil, err
			}
			var users []types.User
			db.Find(&users)
			return users, nil
		},
	}
}

func GetOneUserQuery() *graphql.Field {
	return &graphql.Field{
		Type:        types.UserType,
		Description: "Get one user",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.Int,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			db, err := database.GetDatabase()
			if err != nil {
				return nil, err
			}
			id, ok := params.Args["id"].(int)
			if !ok {
				return nil, errors.New("id must be an integer")
			}
			var user types.User
			db.First(&user, id)
			if user.ID == 0 {
				return nil, errors.New("user not found")
			}
			return user, nil
		},
	}
}
