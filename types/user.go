package types

import (
	"github.com/graphql-go/graphql"
	"gorm.io/gorm"
)

// User type definition.
type User struct {
	gorm.Model
	ID          int    `db:"id" json:"id"`
	Username    string `db:"username" json:"username"`
	Email       string `db:"lastname" json:"lastname"`
	Password    string `db:"password" json:"password"`
	CreatedDate string `db:"created_date" json:"created_date"`
	UpdatedDate string `db:"updated_date" json:"updated_date"`
}

// UserType is the GraphQL schema for the user type.
var UserType = graphql.NewObject(graphql.ObjectConfig{
	Name: "User",
	Fields: graphql.Fields{
		"username": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				return params.Source.(User).Username, nil
			},
		},
		"email": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				return params.Source.(User).Email, nil
			},
		},
		"created_date": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				return params.Source.(User).CreatedDate, nil
			},
		},
		"updated_date": &graphql.Field{
			Type: graphql.String,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				return params.Source.(User).UpdatedDate, nil
			},
		},
	},
})
