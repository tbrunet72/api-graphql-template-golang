package database

import (
	"fmt"

	"gitlab.com/capi2/api/configuration"
	"gitlab.com/capi2/api/types"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

func initDatabase() {
	db, err := GetDatabase()
	if err != nil {
		panic(err)
	}
	if !db.Migrator().HasTable(&types.User{}) {
		db.Migrator().CreateTable(&types.User{})
	}
}

func NewDatabase() (*gorm.DB, error) {
	if db != nil {
		return db, nil
	}
	var database *gorm.DB
	var err error
	cfg, err := configuration.GetConfiguration()
	if err != nil {
		return nil, err
	}

	switch cfg.Database.Dialect {
	case "sqlite":
		database, err = gorm.Open(sqlite.Open(cfg.Database.URL), &gorm.Config{})
	case "postgres":
		database, err = gorm.Open(postgres.Open(cfg.Database.URL), &gorm.Config{})
	default:
		database, err = nil, fmt.Errorf("unsupported dialect: %s", cfg.Database.Dialect)
	}

	if err != nil {
		return nil, err
	}

	db = database
	initDatabase()
	return db, nil
}

func GetDatabase() (*gorm.DB, error) {
	if db != nil {
		return db, nil
	}
	return NewDatabase()
}
