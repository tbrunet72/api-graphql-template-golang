package main

import (
	"log"
	"net/http"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"gitlab.com/capi2/api/configuration"
	"gitlab.com/capi2/api/database"
	"gitlab.com/capi2/api/mutations"
	"gitlab.com/capi2/api/queries"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	_, err := configuration.GetConfiguration()
	if err != nil {
		log.Fatalf("Failed to get configuration, error: %v", err)
	}

	_, err = database.GetDatabase()

	if err != nil {
		log.Fatalf("Failed to get database, error: %v", err)
	}

}

func main() {
	schemaConfig := graphql.SchemaConfig{
		Query: graphql.NewObject(graphql.ObjectConfig{
			Name:   "RootQuery",
			Fields: queries.GetRootFields(),
		}),
		Mutation: graphql.NewObject(graphql.ObjectConfig{
			Name:   "RootMutation",
			Fields: mutations.GetRootFields(),
		}),
	}

	schema, err := graphql.NewSchema(schemaConfig)

	if err != nil {
		log.Fatalf("Failed to create new schema, error: %v", err)
	}

	httpHandler := handler.New(&handler.Config{
		Schema: &schema,
	})

	http.Handle("/", httpHandler)
	log.Print("ready: listening on port 8383\n")

	http.ListenAndServe(":8383", nil)
}
