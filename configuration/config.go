package configuration

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

var cfg *DatabaseConfig

type DatabaseConfig struct {
	Database struct {
		Dialect string `yaml:"dialect"`
		URL     string `yaml:"url"`
	} `yaml:"database"`
}

func NewConfiguration() (*DatabaseConfig, error) {

	if cfg != nil {
		return cfg, nil
	}

	env := os.Getenv("APP_ENV")
	if env == "" {
		env = "dev"
	}
	configFile := fmt.Sprintf("configuration/config.%s.yml", env)
	configData, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(configData, &cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}

func GetConfiguration() (*DatabaseConfig, error) {
	if cfg != nil {
		return cfg, nil
	}
	return NewConfiguration()
}
