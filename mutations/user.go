package mutations

import (
	"errors"
	"time"

	"github.com/graphql-go/graphql"
	"gitlab.com/capi2/api/database"
	"gitlab.com/capi2/api/types"
)

// GetCreateUserMutation creates a new user and returns it.
func GetCreateUserMutation() *graphql.Field {
	return &graphql.Field{
		Type: graphql.Int,
		Args: graphql.FieldConfigArgument{
			"username": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"password": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"email": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			db, err := database.GetDatabase()
			if err != nil {
				return nil, err
			}
			username, ok := params.Args["username"].(string)
			if !ok {
				return nil, errors.New("username must be a string")
			}
			password, ok := params.Args["password"].(string)
			if !ok {
				return nil, errors.New("password must be a string")
			}
			email, ok := params.Args["email"].(string)
			if !ok {
				return nil, errors.New("email must be a string")
			}
			user := types.User{
				Username:    username,
				Password:    password,
				Email:       email,
				CreatedDate: time.Now().String(),
				UpdatedDate: "",
			}
			db.Create(&user)
			return user.ID, nil
		},
	}
}
