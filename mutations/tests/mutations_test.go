package mutations_test

import (
	"fmt"
	"testing"

	"github.com/graphql-go/graphql"
	"gitlab.com/capi2/api/mutations"
)

func TestGetRootFields(t *testing.T) {
	// Define the expected result
	expected := graphql.Fields{
		"createUser": &graphql.Field{
			Type: graphql.Int,
			Args: graphql.FieldConfigArgument{
				"firstname": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"lastname": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				return 1, nil
			},
		},
	}

	// Call the function being tested
	result := mutations.GetRootFields()

	// Check if the result matches the expected value
	if len(result) != len(expected) {
		t.Errorf("Expected %d fields, but got %d", len(expected), len(result))
	}

	for key := range expected {
		expectedField := expected[key]
		resultField, ok := result[key]
		if !ok {
			t.Errorf("Field %s is missing", key)
			continue
		}
		if !(expectedField.Type == resultField.Type) {
			t.Errorf("Expected field %s to have type %s, but got %s", key, expectedField.Type, resultField.Type)
		}
		if len(expectedField.Args) != len(resultField.Args) {
			t.Errorf("Expected field %s to have %d arguments, but got %d", key, len(expectedField.Args), len(resultField.Args))
		}
		for argKey := range expectedField.Args {
			expectedArg := expectedField.Args[argKey]
			resultArg, ok := resultField.Args[argKey]
			if !ok {
				t.Errorf("Argument %s of field %s is missing", argKey, key)
				continue
			}
			fmt.Println(expectedArg.Type, resultArg.Type)
			if expectedArg.Type != resultArg.Type {
				t.Errorf("Expected argument %s of field %s to have type %s, but got %s", argKey, key, expectedArg.Type, resultArg.Type)
			}
		}
		if expectedField.Resolve != nil && resultField.Resolve == nil {
			t.Errorf("Expected field %s to have a resolver function, but got nil", key)
		}
	}
}
