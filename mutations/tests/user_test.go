package mutations_test

import (
	"testing"

	"github.com/graphql-go/graphql"
	"gitlab.com/capi2/api/mutations"
)

func TestGetCreateUserMutation(t *testing.T) {
	// Define the expected result
	expected := 1

	// Define the input parameters
	params := graphql.ResolveParams{
		Args: map[string]interface{}{
			"firstname": "John",
			"lastname":  "Doe",
		},
	}

	// Call the function being tested
	result, err := mutations.GetCreateUserMutation().Resolve(params)

	// Check if the result matches the expected value
	if result != expected {
		t.Errorf("Expected %d, but got %v", expected, result)
	}

	// Check if there was no error
	if err != nil {
		t.Errorf("Expected no error, but got %v", err)
	}
}
